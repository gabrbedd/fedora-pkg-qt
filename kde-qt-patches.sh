# quick-n-dirty method to fetch patches from -patched git branch
# unfortunately, requires an already checked-out copy of the git repo

# when switching branches, use something like: git checkout --track remotes/origin/4.6-stable-patched 
git format-patch --output-directory kde-qt-patches v4.6.1..origin/4.6.1-patched

